import { readdir } from 'fs/promises'
import { SlashCommandWithExecutor } from '../../@types/global'
const PATH = './dist/commands/'

const importCommands = async (path: string)=> {
    const commands: { [x: string]: SlashCommandWithExecutor }[] = []
    try {
        const files = await readdir(path)
        for (const file of files) {
            const commandModule: { [x: string]: SlashCommandWithExecutor } = await import('../commands/' + file)
            // console.log(commandModule)
            commands.push(commandModule)
        }
        return commands
    } catch (err) {
        throw err
    }
}

let commandList = await importCommands(PATH)

export default commandList
