import { REST, Routes } from 'discord.js'
import commandList from './get-slash-command-list.js'
import dotenv from "dotenv"
dotenv.config()

const rest = new REST().setToken(process.env.TOKEN)
const cmd = commandList.map(item => Object.values(item)[0].data)

try {
    console.log(`Started refreshing ${cmd.length} application (/) commands.`)

    // The put method is used to fully refresh all commands in the guild with the current set
    rest.put(Routes.applicationGuildCommands(process.env.CLIENT_ID as string, process.env.GUILD_ID as string), {
        body: cmd,
    })

    console.log(`Successfully reloaded ${cmd.length} application (/) commands.`)
} catch (error) {
    // And of course, make sure you catch and log any errors!
    console.error(error)
}
