import { CacheType, ChannelType, ChatInputCommandInteraction, SlashCommandBuilder } from 'discord.js'
import { SlashCommandWithExecutor } from '../../@types/global'
import emoji from './../assets/emoji.json' assert { type: 'json' }

const ALPHABET = emoji['ALPHABET']

export const CreatePoll: SlashCommandWithExecutor = {
    data: new SlashCommandBuilder()
        .setName('create-poll')
        .setDescription('Create a user poll.')
        .addStringOption((option) =>
            option.setName('poll-question').setDescription('What is the poll for?').setRequired(true)
        )
        .addStringOption((option) =>
            option
                .setName('choices')
                .setDescription('User can select one of the provided options, seperated by semicolons(;)')
                .setRequired(true)
        )
        .addChannelOption((option) =>
            option.setName('channel').setDescription('Choose channel to post the poll in').setRequired(true)
        ),
    executor: async (interaction: ChatInputCommandInteraction<CacheType>): Promise<void> => {
        function pollStringFormat(str: TemplateStringsArray, firstArg: any, secondArg: any | undefined) {
            let alphabet = ALPHABET.slice(0, secondArg.length)
            return (
                str[0] +
                firstArg +
                '\n\n' +
                secondArg
                    .map((item: string, i: number) => alphabet[i] + ' ' + item)
                    .reduce((prev: string, curr: string) => {
                        return `${prev} \n${curr}`
                    })
            )
        }

        // const channelToPost = interaction.options.getString('channel', true)
        const channel = interaction.options.getChannel('channel', true)
        const choices: string[] = interaction.options
            .getString('choices', true)
            .split(/[:,-]/g)
            .map((item) => item.trim())
            .filter((item) => item.length)

        const pollReason = interaction.options.getString('poll-question', true)
        const channelType = interaction.guild?.channels.cache.get(channel.id)
        if (channelType?.type === ChannelType.GuildText) {
            var message = await channelType?.send({ content: pollStringFormat`POLL: ${pollReason}${choices}` })
        }
        await interaction.reply({
            content: `Poll: "${pollReason}" has been successfully created in channel: ${channel.name}`,
            ephemeral: true,
        })

        choices.forEach(async (_, i) => {
            const res = await message.react(ALPHABET[i])
            res ?? console.log('Failed response on reaction with', ALPHABET[i])
        })
    },
}
