console.log('Initializing App!')
import { Client, Events, GatewayIntentBits, Interaction } from 'discord.js'
import dotenv from 'dotenv'
import { init_interactionCreate } from './events/interactionCreate.js'
import { init_messageCreate } from './events/messageCreate.js'

dotenv.config()
const INTENTS = [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageTyping,
]

const client = new Client({ intents: INTENTS })

client.once(Events.ClientReady, async (c) => console.log('Bot Ready as', c.user.tag))

client.login(process.env.TOKEN)

init_messageCreate(client)
init_interactionCreate(client)
