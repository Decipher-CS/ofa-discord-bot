#! /bin/bash
# This script is used inside /home/container on portal.daki.cc which is where the discord bot is hosted.

cd ofa-discord-bot
git fetch
git pull
yarn
node ./dist/main.js

