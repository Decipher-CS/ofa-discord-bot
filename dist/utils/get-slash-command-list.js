import { readdir } from 'fs/promises';
const PATH = './dist/commands/';
const importCommands = async (path) => {
    const commands = [];
    try {
        const files = await readdir(path);
        for (const file of files) {
            const commandModule = await import('../commands/' + file);
            // console.log(commandModule)
            commands.push(commandModule);
        }
        return commands;
    }
    catch (err) {
        throw err;
    }
};
let commandList = await importCommands(PATH);
export default commandList;
