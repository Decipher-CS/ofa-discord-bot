import { Events } from 'discord.js';
// import { client } from '../main.js'
import commandList from '../utils/get-slash-command-list.js';
const cmds = {};
commandList.forEach((item, i) => {
    const properties = Object.values(item)[0];
    if (typeof properties.data.name !== 'undefined') {
        cmds[properties.data.name] = properties;
    }
});
export const init_interactionCreate = (client) => {
    client.on(Events.InteractionCreate, async (interaction) => {
        if (!interaction.isChatInputCommand())
            return;
        const commandName = interaction.commandName;
        await cmds[commandName].executor(interaction);
        // await commandList[commandName].executor(interaction)
        // await commandList.find((cmd) => cmd.name === commandName)?.executor(interaction)
    });
    return 'done';
};
