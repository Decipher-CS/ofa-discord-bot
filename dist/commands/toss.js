import { SlashCommandBuilder } from 'discord.js';
// import { SlashCommandWithExecutor } from '../../@types/global'
export const CoinFlip = {
    data: new SlashCommandBuilder().setName('flip-coin').setDescription('Flip a coin to get heads or tails'),
    executor: async function (interaction) {
        await interaction.reply(Math.floor(Math.random() * 2) ? 'Heads' : 'Tails');
    },
};
