import { SlashCommandBuilder } from 'discord.js'

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            APPLICATION_ID: string
            PUBLIC_KEY: string
            TOKEN: string
            GUILD_ID: string
            CLIENT_ID: string
            PORT: number
        }
    }
}

export interface SlashCommandWithExecutor {
    executor: (interaction: ChatInputCommandInteraction<CacheType>) => Promise<void>
    data: Partial<SlashCommandBuilder>
}

export {}
